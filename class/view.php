<?php

namespace pongsit\view;
	
class view{
	
	private $pageName = '';
	private $path_to_root = '';
	private $path_to_core = '';
	private $path_to_vendor = '';
	private $path_to_app = '';
	private $app = '';
	
	public function __construct($pageName="") 
    {
    	$this->path_to_root = $GLOBALS['path_to_root'];
	    $this->path_to_core = $GLOBALS['path_to_root'].'vendor/pongsit/';
	    $this->path_to_vendor = $GLOBALS['path_to_root'].'vendor/';
	    $this->path_to_app = $GLOBALS['path_to_root'].'app/';
		$this->app = $GLOBALS['app'];
		
	    // if there is a specific view to load, load that one
	    // else load the default which is the same name as the current page
	    if(!empty($pageName)){
		    $this->pageName = $pageName;
	    }else{
		    // extract the php to get current page name
		    $this->pageName = basename($_SERVER['PHP_SELF'],'.php');
	    }
    }
    
    // load the content of a file
    function load($path){
		$content = '';
		
		if(file_exists($this->path_to_app.$this->app.'/'.$path)){
			$content = file_get_contents($this->path_to_app.$this->app.'/'.$path);
		}else{
			if(file_exists($this->path_to_core.$this->app.'/'.$path)){
				$content = file_get_contents($this->path_to_core.$this->app.'/'.$path);
			}
		}
		
		if(empty($content)){
			if(file_exists($this->path_to_app.'system/'.$path)){
				$content = file_get_contents($this->path_to_app.'system/'.$path);
			}else{
				if(file_exists($this->path_to_core.'system/'.$path)){
					$content = file_get_contents($this->path_to_core.'system/'.$path);
				}
			}
		}
		
		if(empty($content)){ error_log('No html for: '.$path); }
		
		return $content;
    }
    
    // replace placeholders with variables
    function replaceVariable($variables=array(),$content){
	    // if variable is not empty
	    if(!empty($variables)){
		    // replace the placeholders with each variable 
			foreach($variables as $key => $value){
				if(is_array($value)){ continue; }
				$content = str_replace('{{'.$key.'}}', $value, $content);
			}
		}
		return $content;
    }
    
    function replace_variable($variables=array(),$content){
	    return $this->replaceVariable($variables,$content);
    }
    
    function replace_variable_append_id($id,$content){
	    if(!empty($id)){
			preg_match_all('#{{(.*?)}}#', $content, $matches);
			foreach($matches[1] as $value){
				$content = $this->replaceVariable(array($value=>'{{'.$value.'-'.$id.'}}'),$content);
			}
		}
		return $content;
    }
    function fill_empty($content){
		preg_match_all('#{{(.*?)}}#', $content, $matches);
		foreach($matches[1] as $value){
			$content = str_replace('{{'.$value.'}}', '', $content);
		}
		return $content;
    }
	
	// create view
	function create($variables=array(),$fill_empty=false){	
	
		$systems=array();
		$systems['head'] = '';
		if(!empty($variables['head'])){
			$systems['head'] = $variables['head'];
		}
		if(!empty($variables['header'])){
			$systems['header'] = $variables['header'];
		}else{
			if(!empty($_SESSION['user']['username']) && !empty($_SESSION['user']['id'])){
				$systems['header'] = $this->block('header');
			}else{
				$systems['header'] = $this->block('header-anonymous');
			}
		}
		if(!empty($variables['footer'])){
			$systems['footer'] = $variables['footer'];
		}else{
			if((!empty($_SESSION['user']['username']) && !empty($_SESSION['user']['id']))){
				$systems['footer'] = $this->block('footer-after-login');
			}else{
				$systems['footer'] = $this->block('footer');
			}
		}
		
		$systems['main'] = $this->block('main');
		
		// load html content associated with page
		$html = $this->load('html/'.$this->pageName.'.html');
		
		// if there is no view for this page, show 404 page - Page not found! 
		if(empty($html)){
			$html = $this->load('html/404.html');
		}
		
		$systems['content'] = $html;
		
		$systems['main'] = $this->replaceVariable($systems, $systems['main']);
		
		if(empty($variables['h1'])){
			$variables['h1'] = '';
		}else{
			$systems['main'] = $this->replaceVariable(array('h1'=>$variables['h1']),$systems['main']);
			unset($variables['h1']);
		}
		
		if(empty($variables['f1'])){
			$variables['f1'] = '';
		}
		
		if(empty($variables['page-name'])){
			$variables['page-name'] = '';
		}
		
		if(empty($variables['notification'])){
			$variables['notification'] = '';
		}
		
		if(empty($variables['navigation'])){
			$variables['navigation'] = '';
		}
		
		// replace placeholders with variables
		$systems['main'] = $this->replaceVariable($variables, $systems['main']);
		
		// replace placeholders with global variables
		if(!empty($GLOBALS['authVariables'])){
			$systems['main'] = $this->replaceVariable($GLOBALS['authVariables'], $systems['main']);
		}
		// replace placeholders with global variables
		if(!empty($GLOBALS['confVariables'])){
			$systems['main'] = $this->replaceVariable($GLOBALS['confVariables'], $systems['main']);
		}

		if($fill_empty){
			$systems['main'] = $this->fill_empty($systems['main']);
		}
		
		return $systems['main'];
	}
	
	function block($name,$variables=array()){
		if(empty($variables['class'])){
			$variables['class'] = '';
		}
		$content = $this->load('html/block/'.$name.'.html');
		$content = $this->replaceVariable($variables,$content);
		return $content;
	}
	
	function ajax($name,$variables=array()){
		$content = $this->load('html/ajax/'.$name.'.html');
		$variables['ajax_id']=rand(1000,9999);
		if(!empty($variables['display_pattern'])){
			$variables['display_pattern'] = $this->replace_variable_append_id($variables['ajax_id'],$variables['display_pattern']);
		}
		if(empty($variables['call_back'])){
			$variables['call_back']='';
		}
		if(empty($variables['css'])){
			$variables['css']='';
		}
		if(empty($variables['placeholder'])){
			$variables['placeholder']='';
		}
		if(empty($variables['json'])){
			$variables['json']='{}';
		}else{
			$jsons = json_decode($variables['json']);
			$variables['limit']=@$jsons->limit;
			$variables['offset']=@$jsons->offset;
		}
		if(empty($variables['offset'])){
			$variables['offset']=0;
		}
		if(empty($variables['limit'])){
			$variables['limit']=-1;
		}
		$content = $this->replaceVariable($variables,$content);
		return $content;
	}
	
	function exist($view_name){
		$content = '';
		$content = $this->load('html/'.$view_name.'.html');
		if(!empty($content)){
			return true;
		}
		return false;
	}

}