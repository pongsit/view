var view = {};

view.content = '';

view.prep = function(content, variables){
	variables.path_to_root = path_to_root;
	variables.path_to_core = path_to_core;
	variables.path_to_vendor = path_to_vendor;
	variables.path_to_app = path_to_app;
	$.each(variables,function(key,val){
		if(!$.isNumeric(val)){
			if(!val){
				val = '';
			}
		}
		if(!$.isNumeric(key)){
			content = content.replace(new RegExp('{{'+key+'}}', 'g'), val);
		}
	});
	return content;
};

view.insert = function(method,id_tag_from,id_tag_to,content,variables,callback){
	switch(method){
		case 'create': $('#'+id_tag_to).show().html(view.prep(content, variables)); break;
		case 'append': $('#'+id_tag_to).show().append(view.prep(content, variables)); break;
		case 'prepend': $('#'+id_tag_to).show().prepend(view.prep(content, variables)); break;
	}
	if(typeof callback === "function"){
		callback();
	}
}

view.create = function(id_tag_from,id_tag_to,variables,callback){
	var command = $.get(path_to_root+"app/"+app+"/html/"+id_tag_from+".html?rand="+Math.random(),{dataType : 'html'}, 
		function(content) {
			view.insert('create',id_tag_from,id_tag_to,content,variables,callback);
		}
	);
	command.fail(function(){
		$.get(path_to_app+app+"html/"+id_tag_from+".html?rand="+Math.random(),{dataType : 'html'}, 
			function(content) {
				view.insert('create',id_tag_from,id_tag_to,content,callback);
			}
		);
	});
}

view.append = function(id_tag_from,id_tag_to,variables,callback){
	if(!app){ app = 'system'; }
	var command = $.get(path_to_app+app+"/html/"+id_tag_from+".html?rand="+Math.random(),{dataType : 'html'}, 
		function(content) {
			view.insert('append',id_tag_from,id_tag_to,content,variables,callback);
		}
	);
	command.fail(function(){
		$.get(path_to_vendor+"pongsit/"+app+"/html/"+id_tag_from+".html?rand="+Math.random(),{dataType : 'html'}, 
			function(content) {
				view.insert('append',id_tag_from,id_tag_to,content,variables,callback);
			}
		);
	});
}

view.prepend = function(id_tag_from,id_tag_to,variables,callback){
	if(!app){ app = 'system'; }
	var command = $.get(path_to_root+"app/"+app+"/html/"+id_tag_from+".html?rand="+Math.random(),{dataType : 'html'}, 
		function(content) {
			view.insert('prepend',id_tag_from,id_tag_to,content,variables,callback);
		}
	);
	command.fail(function(){
		$.get(path_to_app+app+"/html/"+id_tag_from+".html?rand="+Math.random(),{dataType : 'html'}, function(content) {
				view.insert('prepend',id_tag_from,id_tag_to,content,callback);
			}
		);
	});
}

view.prep_add = function(content,unique,variables){
	$.each(variables,function(key,val){
		if(!val){
			val = '';
		}
		if(!$.isNumeric(key)){
			content = content.replace(new RegExp('{{'+key+'-'+unique+'}}', 'g'), val);
		}
	});
	return content;
};

view.add = function(id_tag_from,id_tag_to,unique,variables,callback){
	var content = $("#"+id_tag_from).html();
	if(content){
		$('#'+id_tag_to).show().append(view.prep_add(content,unique,variables));
	}
	if(typeof callback === "function"){
		callback();
	}
}

view.self = function(id_tag_from,unique,variables){
	var content = $("#"+id_tag_from).html();
	if(content){
		return view.prep_add(content,unique,variables);
	}
}

view.block = function(id_tag_from,id_tag_to,variables,callback){
	if(!app){ app = 'system'; }
	var command = $.get(path_to_root+"app/"+app+"/html/block/"+id_tag_from+".html?rand="+Math.random(),{dataType : 'html'}, 
		function(content) {
			view.insert('append',id_tag_from,id_tag_to,content,variables,callback);
		}
	);
	command.fail(function(){
		alert(path_to_app+app+"/html/block/"+id_tag_from+".html");
		$.get(path_to_app+app+"/html/block/"+id_tag_from+".html?rand="+Math.random(),{dataType : 'html'}, 
			function(content) {
				view.insert('append',id_tag_from,id_tag_to,content,variables,callback);
			}
		);
	});
}
view.block_prepend = function(id_tag_from,id_tag_to,variables,callback){
	if(!app){ app = 'system'; }
	var command = $.get(path_to_root+"app/"+app+"/html/block/"+id_tag_from+".html?rand="+Math.random(),{dataType : 'html'}, 
		function(content) {
			view.insert('prepend',id_tag_from,id_tag_to,content,variables,callback);
		}
	);
	command.fail(function(){
		alert(path_to_app+app+"/html/block/"+id_tag_from+".html");
		$.get(path_to_app+app+"/html/block/"+id_tag_from+".html?rand="+Math.random(),{dataType : 'html'}, 
			function(content) {
				view.insert('prepend',id_tag_from,id_tag_to,content,variables,callback);
			}
		);
	});
}